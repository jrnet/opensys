package com.example.opensys.seivices;

import com.example.opensys.constant.Constant;
import com.example.opensys.mapper.BlogBookModelMapper;
import com.example.opensys.model.BlogBookModel;
import com.example.opensys.model.BlogBookTypeModel;
import com.example.opensys.model.SystemTokenModel;
import com.example.opensys.utils.IdUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;
import java.text.SimpleDateFormat;

@Service
public class DeskBlogService extends AbstractService<BlogBookModelMapper,BlogBookModel>{

//    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    Calendar calendar = Calendar.getInstance();


    @Autowired
    private UserService userService;
    @Autowired
    private SysService sysService;
    @Autowired
    private BlogBookTypeService blogBookTypeService;
    /**
     * 获取博客文章列表
     * @param page
     * @param limit
     * @param title
     * @param content
     * @return
     */
    public PageInfo<BlogBookModel>  finBlogBookList(Integer page, Integer limit , String title, String content){
        Example example = new Example(BlogBookModel.class);
        Example.Criteria criteria = example.createCriteria();
        // 标题
        if (StringUtil.isNotEmpty(title)){
            criteria.andLike("title", "%"+title+"%");
        }
        // 内容
        if (StringUtil.isNotEmpty(content)){
            criteria.andLike("content", "%"+content+"%");
        }
        // 倒序
        example.setOrderByClause("creatDateTime desc");
        PageInfo<BlogBookModel> pageInfo = find(example,page,limit);
        if(pageInfo.getTotal()>0){
            for (BlogBookModel modelList: pageInfo.getList()){
                // 用户名称
                modelList.setRealName(userService.finByUidName(modelList.getCreatuid()).getName());
                if (blogBookTypeService.finBlogBookTypeOne(modelList.getType())!=null){
                    // 分离名称
                    modelList.setType(blogBookTypeService.finBlogBookTypeOne(modelList.getType()).getLabel());
                }
            }
        }
        return  pageInfo;
    }

    /**
     * 获取博客文章信息
     * @param id 文章id
     * @return
     */
    public BlogBookModel finBlogBookInfo(String id){
        // 查询用户的名称
        BlogBookModel blogBookModel = getById(id);
        String name = userService.finByUidName(blogBookModel.getCreatuid()).getName();
        BlogBookTypeModel blogBookTypeModel = blogBookTypeService.finBlogBookTypeOne(blogBookModel.getType());
        blogBookModel.setRealName(name);
        if (blogBookTypeModel!=null){
            blogBookModel.setType(blogBookTypeModel.getLabel());
        }
        return blogBookModel;
    }

    /**
     * 新增博客
     * @param blogBookModel 参数
     * @return
     */
    public int addBlogBook(BlogBookModel blogBookModel){
        BlogBookModel blogBookModel1 = blogBookModel;
        blogBookModel1.setId(IdUtils.getUUID());
        blogBookModel1.setCreatdatetime(new Date());
        blogBookModel1.setCreatuid(sysService.getToken(Constant.TOKEN).getUserId());
        return insert(blogBookModel1);
    }

    /**
     * 修改博客
     * @param blogBookModel 参数
     * @return
     */
    public int updateBlogBook(BlogBookModel blogBookModel){
        BlogBookModel blogBookModel1 = blogBookModel;
        // 1根据token 拿到用户名 2判断用户是否存在 3存在则拿到相应id
//        blogBookModel1.setUpdateuid(userService.finUserName().getUid());
        blogBookModel1.setUpdatedatetime(new Date());
        return insert(blogBookModel1);
    }

    /**
     * 删除博客
     * @param ids 文章id
     * @return
     */
    public List<String> delBlogBookInfo(List<String> ids){
        // 查询用户的名称
        List<String> blogBookModel = batchByIds(ids);
        return  blogBookModel;
    }

    public static String getUUID(){
        UUID uuid=UUID.randomUUID();
        String str = uuid.toString();
        String uuidStr=str.replace("-", "");
        return uuidStr;
    }
}
