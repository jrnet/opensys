package com.example.opensys.seivices;

import com.example.opensys.mapper.UserModelMapper;
import com.example.opensys.model.UserModel;
import com.example.opensys.model.po.UserPo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired()
    private UserModelMapper userMapper;

    public UserModel finUserName(String userName){
        return userMapper.finUserName(userName);
    }

    public UserModel finByUidName(String uid){
        return userMapper.finByUidName(uid);
    }

    public List<UserPo> finUserList(){
        List<UserModel> userList = userMapper.finUserList();
        List<UserPo> userPoList = new ArrayList<>();
        for (int i=0;i<userList.size() ;i++){
            UserPo userPo = new UserPo();
            BeanUtils.copyProperties(userList.get(i),userPo);
            userPoList.add(userPo);
        }
        return userPoList;
    }

    public int addUser(UserModel user){
        return  userMapper.addUser();
    }



}
