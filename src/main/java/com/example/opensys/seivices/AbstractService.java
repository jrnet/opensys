package com.example.opensys.seivices;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

abstract public class AbstractService<M extends Mapper<E>, E extends Object>{
    @Autowired
    protected M mapper;
    public AbstractService() {
    }

    /**
     * 分页获取数据 条件
     *
     * @param pageNum   页码
     * @param limit     条数
     * @param search    条件
     * @return
     */
    public PageInfo<E> findPageLimit(Integer pageNum, Integer limit ,E search) {
        PageHelper.startPage(pageNum, limit);
        List<E> list = search!=null && search!="" ? mapper.select(search): mapper.selectAll();
        PageInfo<E> page = new PageInfo<>(list);
        return page;
    }


    /**
     * 分页查询根据example进行条件筛选
     *
     * @param example
     * @param pageNum
     * @param limit
     * @return
     */
    public PageInfo<E> find(Example example, Integer pageNum, Integer limit) {
        PageHelper.startPage(pageNum, limit);
        List<E> list = mapper.selectByExample(example);
        PageInfo<E> page = new PageInfo<>(list);
        return page;
    }

    /**
     * 分页查询全部
     *
     * @param pageNum
     * @param limit
     * @return
     */
    public PageInfo<E> findAllPage(Integer pageNum,Integer limit) {
        PageHelper.startPage(pageNum, limit);
        List<E> list = mapper.selectAll();
        PageInfo<E> page = new PageInfo<>(list);
        return page;
    }

    /**
     * 不分页查询全部
     *
     * @return
     */
    public List<E> findAll() {
        return mapper.selectAll();
    }

    /**
     * 不分页按条件查询
     *
     * @return
     */
    public List<E> findRecord(E record) {
        return mapper.select(record);
    }

    /**
     * 不分页按条件查询
     *
     * @return
     */
    public List<E> findRecord(Example example) {
        return mapper.selectByExample(example);
    }

    /**
     * 根据example进行条件筛选单个
     *
     * @param example
     * @return
     */
    public E get(Example example) {
        return mapper.selectOneByExample(example);
    }

    /**
     * 查询数据通过id
     *
     * @param id
     * @return
     */
    public E getById(String id) {
        return mapper.selectByPrimaryKey(id);
    }

    /**
     * 插入数据
     *
     * @param
     * @return
     */
    public int insert(E e) {
        return mapper.insert(e);
    }

    /**
     * 修改数据
     *
     * @param
     * @return
     */
    public int updata(E e) {
        return mapper.updateByPrimaryKey(e);
    }

    /**
     * 修改数据
     *
     * @param
     * @return
     */
    public int updata(E e,Example example) {
        return mapper.updateByExample(e,example);
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    public int delById(String id) {
        return mapper.deleteByPrimaryKey(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    public List<String> batchByIds(List<String> ids) {
        List<String> stringList = new ArrayList<>();
        for (String id:ids){ 
            if (delById(id) == 0) stringList.add(id);
        }
        return  stringList;
    }

}
