package com.example.opensys.seivices;

import com.example.opensys.constant.Constant;
import com.example.opensys.mapper.BlogBookTypeModelMapper;
import com.example.opensys.model.BlogBookTypeModel;
import com.example.opensys.utils.IdUtils;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.bcel.internal.generic.LADD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class BlogBookTypeService extends AbstractService<BlogBookTypeModelMapper, BlogBookTypeModel>{

    @Autowired
    private  SysService sysService;
    /**
     * 获取全部类别
     * @return
     */
    public List<BlogBookTypeModel> finAllBlogBookType(){
        Example example = new Example(BlogBookTypeModel.class);
        Example.Criteria criteria = example.createCriteria();
        // 排除已删除的
        criteria.andLike("state","1");
        criteria.orLike("state","2");
        List<BlogBookTypeModel> blogBookTypeModels = findRecord(example);
        return blogBookTypeModels;
    }

    /**
     * 获取全部类别-分页
     * @return
     */
    public PageInfo<BlogBookTypeModel> finAllBlogBookTypePage(Integer page,Integer limit){
        PageInfo<BlogBookTypeModel> blogBookTypeModels = findAllPage(page, limit);
        return blogBookTypeModels;
    }

    /**
     * 根据ID获取单个类别
     * @param id
     * @return
     */
    public BlogBookTypeModel finBlogBookTypeOne(String id){
        BlogBookTypeModel blogBookTypeModels = getById(id);
        return blogBookTypeModels;
    }

    /**
     * 根据ID更新类别
     * @param id
     * @param status
     * @return
     */
    public Integer updateBookStatus(String id,Integer status){
        BlogBookTypeModel blogBookTypeModel = finBlogBookTypeOne(id);
        blogBookTypeModel.setState(status);
        Integer blogBookTypeModels = updata(blogBookTypeModel);
        return blogBookTypeModels;
    }

    /**
     * 添加分类
     * @param label
     * @return
     */
    public Integer addType(String label){
        BlogBookTypeModel blogBookTypeModel = new BlogBookTypeModel();
        blogBookTypeModel.setLabel(label);
        blogBookTypeModel.setId(IdUtils.getUUID());
        blogBookTypeModel.setState(1);
        blogBookTypeModel.setCreatdatetime(new Date());
        blogBookTypeModel.setCreatuid(sysService.getToken(Constant.TOKEN).getUserId());
        Integer blogBookTypeModels = insert(blogBookTypeModel);
        return blogBookTypeModels;
    }


    /**
     * 根据ID获取删除
     * @param ids
     * @return
     */
    public List<String> blogDels(List<String> ids){
        List<String> blo = batchByIds(ids);
        return blo;
    }
}
