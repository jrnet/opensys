package com.example.opensys.seivices;

import com.example.opensys.mapper.SystemTokenModelMapper;
import com.example.opensys.model.SystemTokenModel;
import com.example.opensys.model.UserModel;
import com.example.opensys.model.po.UserPo;
import com.example.opensys.utils.DateTimeUtils;
import com.example.opensys.utils.EncryptUtils;
import com.example.opensys.utils.IdUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;
import com.example.opensys.constant.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class SysService extends AbstractService<SystemTokenModelMapper, SystemTokenModel> {

    @Autowired
    private UserService userService;
    /**
     * 获取token
     *
     * @return token实体
     */
    public SystemTokenModel getToken(String token) {
        Example example = new Example(SystemTokenModel.class);
        Example.Criteria criteria = example.createCriteria();
        // 标题
        if (StringUtil.isNotEmpty(token)) {
            criteria.andLike("token", token);
        }
        SystemTokenModel sysTokenModel = get(example);
        return sysTokenModel;
    }

    /**
     * 创建token
     *
     * @return token信息
     */
    public UserPo createToken(String userId) {
        //查询用户信息
        UserModel sysUser = userService.finByUidName(userId);

        UserPo userPo = new UserPo();
        userPo.setToken(createToken(sysUser));
        // 使用BeanUtils工具 复制一份给po
        BeanUtils.copyProperties(sysUser,userPo);
        return userPo;
    }

    private SystemTokenModel createToken(UserModel userModel) {
        SystemTokenModel sysTokenModel = new SystemTokenModel();
        // 生成token
        String token = EncryptUtils.passwordEncrypt(userModel.getPassword() + new Date().getTime());
        sysTokenModel.setCreateTime(new Date());
        sysTokenModel.setId(IdUtils.getId());
        sysTokenModel.setStatus(Constant.USER_LOGIN_STATIC);
        sysTokenModel.setUserId(userModel.getUid());
        sysTokenModel.setLoginTime(new Date());
        sysTokenModel.setToken(token);
        // 创建成功
        if (insert(sysTokenModel) == 1) {
            return sysTokenModel;
        }
        // 创建失败
        return null;
    }
}
