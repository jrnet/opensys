package com.example.opensys.seivices;

import com.example.opensys.mapper.BlogBookTagModelMapper;
import com.example.opensys.model.BlogBookTagModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlogBookTagService extends AbstractService<BlogBookTagModelMapper, BlogBookTagModel>{
    /**
     * 获取全部标签
     */
    public List<BlogBookTagModel> findBlogBookTagAll(){
        List<BlogBookTagModel> bookTagModelList = new ArrayList<>();
        return bookTagModelList;
    }

    /**
     * 获取单个标签
     */
    public BlogBookTagModel findBlogBookTagOne(){
        BlogBookTagModel bookTagModel = new BlogBookTagModel();
        return bookTagModel;
    }
}
