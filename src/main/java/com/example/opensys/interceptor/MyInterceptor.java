package com.example.opensys.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.opensys.constant.Constant;
import com.example.opensys.model.SystemTokenModel;
import com.example.opensys.seivices.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 *
 *
 * @Package: com.example.opensys.interceptor;
 * @ClassName: MyInterceptor
 * @Description: 创建拦截器
 * @author: zwb
 * @date:
 */
@Component
public class MyInterceptor implements HandlerInterceptor {

    @Autowired
    private SysService sysService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        String token = request.getHeader("token");
        return  true;
//        System.out.println("------>\n请求的地址："+request.getRequestURI() + "\n请求的token:"+token+"------>\n");
//        if (token!=null) {
//            SystemTokenModel sysToken = sysService.getToken(token);
//            if (sysToken != null){
//                // 有token，判断token是否有效
//                if (sysToken.getStatus().equals(1)){
//                    Constant.TOKEN = token;
//                    return  true;
//                }
//            }
//        }
//        //当session为空时,则以json格式返回403状态
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json; charset=utf-8");
//        response.sendRedirect("/sys/notToken");
//        return false;
    }
}
