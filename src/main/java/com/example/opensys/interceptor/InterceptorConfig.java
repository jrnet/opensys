package com.example.opensys.interceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * 拦截器配置
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Value("${file.uploadFolder}")
    private String mImagesPath;

    // 注册拦截器前 将Bean注入进来 否装拦截器无法注入service
    @Bean
    MyInterceptor getInterceptorConfig() {
        return new MyInterceptor();
    }
        // 拦截器配置
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册Interceptor拦截器，使用已经注入bean的getInterceptorConfig
        InterceptorRegistration registration = registry.addInterceptor(getInterceptorConfig());
        //所有路径都被拦截
        registration.addPathPatterns("/**");
        //添加不拦截路径
        registration.excludePathPatterns(
                "/",
                "/blog/getBlogAll",
                "/blog/getBlogInfo",
                "/sys/login",
                "/sys/notToken",
                "/uploads/**",
                "/static/**",
                "/sys/code"
        );
    }

    /**
     * 静态资源访问
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //上传文件的路径映射
        registry.addResourceHandler("/uploads/**").addResourceLocations("file:"+mImagesPath);
    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //设置允许跨域的路径
        registry.addMapping("/**")
                //设置允许跨域请求的域名
                .allowedOrigins("*")
                //是否允许证书 不再默认开启
                .allowCredentials(true)
                //设置允许的方法
                .allowedMethods("*")
                .allowedHeaders("*")
                //跨域允许时间
                .maxAge(3600);
    }
}
