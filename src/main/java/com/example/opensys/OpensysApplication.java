package com.example.opensys;

import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan({"com.example.opensys.mapper"})
@EnableSwagger2
public class OpensysApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpensysApplication.class, args);
//        setApiDoc();
    }

    private static void setApiDoc() {
        String path = System.getProperty("user.dir");
        System.out.print("---------------"+path);
        DocsConfig config = new DocsConfig();
        config.setProjectPath(path); // 项目根目录
        config.setProjectName("opensys"); // 项目名称
        config.setApiVersion("V1.0");       // 声明该API的版本
        config.setDocsPath("src/main/resources/static/docs"); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        Docs.buildHtmlDocs(config); // 执行生成文档
    }



}