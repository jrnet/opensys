package com.example.opensys.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 单例模式的 时间日期工具
 */
public class DateTimeUtils {
    Calendar calendar = Calendar.getInstance();

    private static class SingletonClassInstance{
        private static final DateTimeUtils instance=new DateTimeUtils();
    }

    private DateTimeUtils(){}

    public static DateTimeUtils getInstance(){
        return SingletonClassInstance.instance;
    }

    /**
     * @return 当前时间
     */
    public Date getDateTimeNow(){
        return  calendar.getTime();
    }
}
