package com.example.opensys.utils;

import java.util.*;

public class CollectionUtils extends org.springframework.util.CollectionUtils {
    /**
     * check the collection is not empty
     *
     * @param collection
     * @return
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !CollectionUtils.isEmpty(collection);
    }

    /**
     * check the collection is not empty
     *
     * @param collection
     * @return
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !CollectionUtils.isEmpty(map);
    }

    /**
     *
     * @param collection
     * @param o
     * @param fieldName
     * @return
     */
    public static <T> boolean contain(Collection<T> collection, T o) {
        return contain(collection, o, "id");
    }

    /**
     *
     * @param collection
     * @param o
     * @param fieldName
     * @return
     */
    public static <T> boolean contain(Collection<T> collection, T o,
                                      String fieldName) {
        boolean found = false;
        if (isEmpty(collection) || o == null) {
            return found;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            T a = it.next();
            if (ReflectUtils.getValueByFieldName(a, fieldName).equals(
                    ReflectUtils.getValueByFieldName(o, fieldName))) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     *
     * @param collection
     * @param o
     * @param fieldName
     * @return
     */
    public static <T> T find(Collection<T> collection, T o) {
        return find(collection, o, "id");
    }

    /**
     *
     * @param collection
     * @param o
     * @param fieldName
     * @return
     */
    public static <T> T find(Collection<T> collection, T o, String fieldName) {
        T t = null;
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            T a = it.next();
            if (ReflectUtils.getValueByFieldName(a, fieldName).equals(
                    ReflectUtils.getValueByFieldName(o, fieldName))) {
                t = a;
                break;
            }
        }
        return t;
    }

    /**
     * 随机排序
     *
     * @param list
     */
    public static <T> void shuffle(List<T> list) {
        Collections.shuffle(list);
    }
}
