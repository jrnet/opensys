package com.example.opensys.utils;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class EncryptUtils {
    /**
     * 密码加密
     *
     * @param plainText
     * @return
     */
    public static String passwordEncrypt(String plainText) {
        return passwordEncrypt(plainText, "#", "#");
    }

    /**
     * 密码加密
     *
     * @param plainText
     * @param prefix
     * @param postfix
     * @return
     */
    public static String passwordEncrypt(String plainText, String prefix,
                                         String postfix) {
        String encryptPassword = null;
        if (plainText != null) {
            encryptPassword = StringUtils.md5Hex(prefix + plainText + postfix);
        }
        return encryptPassword;
    }

    /**
     *
     * @param src
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String md5Digest(String src) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] b = md.digest(src.getBytes("utf-8"));
        return byte2HexStr(b);
    }

    /**
     *
     * @param b
     * @return
     */
    private static String byte2HexStr(byte[] b) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < b.length; ++i) {
            String s = Integer.toHexString(b[i] & 0xFF);
            if (s.length() == 1)
                sb.append("0");

            sb.append(s.toUpperCase());
        }
        return sb.toString();
    }
}
