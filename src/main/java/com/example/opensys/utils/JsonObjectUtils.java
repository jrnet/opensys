package com.example.opensys.utils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.example.opensys.base.ResultBeanModel;

public class JsonObjectUtils {

    public static JSONObject jsonDataModelToJson(ResultBeanModel dataModel){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data",dataModel.getData());
        jsonObject.put("msg",dataModel.getMsg());
        jsonObject.put("code",dataModel.getCode());
        return jsonObject;
    }

    /**
     * 转换成Json输出
     * @param object
     * @return
     */
    public static JSONObject toJson(Object object){
        return (JSONObject) JSONObject.toJSON(object);
    }

    /**
     * 把null转换为""的json格式输出
     * @param jsonObject
     * @return
     */
    public static String jsonNullStringAsEmpty(JSONObject jsonObject){
        return JSONObject.toJSONString(jsonObject, SerializerFeature.WriteNullStringAsEmpty);
    }

    /**
     * 把null转换为""的json格式输出
     * @param dataModel
     * @return
     */
    public static String jsonNullStringAsEmpty(ResultBeanModel dataModel){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("data",dataModel.getData());
        jsonObject.put("msg",dataModel.getMsg());
        jsonObject.put("status",dataModel.getCode());
        return JSONObject.toJSONString(jsonObject, SerializerFeature.WriteNullStringAsEmpty);
    }


}
