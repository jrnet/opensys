package com.example.opensys.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class LogUtils {
    private static Logger logger = LoggerFactory.getLogger("ISYSTEM");

    /**
     * 信息日志
     *
     * @param message
     */
    public static void debug(String message) {
        logger.debug(message);
    }

    /**
     * 信息日志
     *
     * @param message
     */
    public static void info(String message) {
        logger.info(message);
    }

    /**
     * 信息日志
     *
     * @param message
     */
    public static void warn(String message) {
        logger.warn(message);
    }

    /**
     * 信息日志
     *
     * @param message
     */
    public static void error(String message) {
        logger.error(message);
    }

    /**
     * 信息日志
     *
     * @param e
     */
    public static void error(Exception e) {
        StringBuffer sb = new StringBuffer();
        StackTraceElement[] trace = e.getStackTrace();
        for (StackTraceElement s : trace) {
            sb.append("\tat " + s + "\r\n");
        }
        logger.error(sb.toString());
    }
}