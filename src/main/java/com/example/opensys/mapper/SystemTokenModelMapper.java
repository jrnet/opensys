package com.example.opensys.mapper;

import com.example.opensys.model.SystemTokenModel;
import tk.mybatis.mapper.common.Mapper;

public interface SystemTokenModelMapper extends Mapper<SystemTokenModel> {
}