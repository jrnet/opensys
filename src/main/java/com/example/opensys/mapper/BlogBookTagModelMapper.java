package com.example.opensys.mapper;

import com.example.opensys.model.BlogBookTagModel;
import tk.mybatis.mapper.common.Mapper;

public interface BlogBookTagModelMapper extends Mapper<BlogBookTagModel> {
}