package com.example.opensys.mapper;

import com.example.opensys.model.BlogBookModel;
import tk.mybatis.mapper.common.Mapper;

public interface BlogBookModelMapper extends Mapper<BlogBookModel> {
}