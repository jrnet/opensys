package com.example.opensys.mapper;

import com.example.opensys.model.BlogBookTypeModel;
import tk.mybatis.mapper.common.Mapper;

public interface BlogBookTypeModelMapper extends Mapper<BlogBookTypeModel> {
}