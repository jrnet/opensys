package com.example.opensys.mapper;

import com.example.opensys.model.UserModel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface UserModelMapper extends Mapper<UserModel> {
    @Select("SELECT * FROM user WHERE username=#{username}")
    public  UserModel finUserName(@Param("username")String username);

    @Select("SELECT * FROM user WHERE uid=#{uid}")
    public  UserModel finByUidName(@Param("uid")String uid);

    @Select("SELECT * FROM user")
    public List<UserModel> finUserList();

    @Select("")
    public  int delUser();

    @Select("")
    public  int addUser();

    @Select("")
    public  int updateUser();
}