package com.example.opensys.base;

import com.alibaba.fastjson.JSONObject;
import com.example.opensys.controller.BaseController;
import com.example.opensys.controller.SysController;
import io.github.yedaxia.apidocs.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Map;

/**
 * 设置全局异常
 */
@Ignore
@Configuration
@RestControllerAdvice
@ResponseBody
@RestController
public class GlobalExceptionHandler extends BaseController implements ErrorController {
    private final ErrorAttributes errorAttributes;
    private static final String ERROR_PATH = "/error";
    private static final Logger logger = LoggerFactory.getLogger(SysController.class);

    /**
     * 约束异常捕获
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public JSONObject handler(HttpServletRequest request, ConstraintViolationException ex) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for (ConstraintViolation violation : ex.getConstraintViolations()) {
            sb.append((++i == 1 ? "" : ",") + violation.getMessage());
        }
        logger.info(sb.toString());
        return failure(sb.toString());
    }

    @ExceptionHandler(BindException.class)
    @ResponseBody
    public JSONObject bindExceptionHandler(BindException ex) {
        StringBuffer sb = new StringBuffer();
        BindingResult bindingResult = ex.getBindingResult();
        if (bindingResult.hasErrors()) {
            for (int i = 0; i < bindingResult.getAllErrors().size(); i++) {
                ObjectError error = bindingResult.getAllErrors().get(i);
                sb.append((i == 0 ? "" : "\n") + error.getDefaultMessage());
            }
        }
        return failure(sb.toString());
    }

    /**
     * 设置检验模式,一个校验失败,其他就不必检验
     *
     * @return
     */
    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        Validator validator = (Validator) validatorFactory.getValidator();
        return validator;
    }


    @Ignore
    @RequestMapping(value = ERROR_PATH)
    public JSONObject handleError(WebRequest request) {
        //获取请求错误信息
        Map<String, Object> attr = this.errorAttributes.getErrorAttributes(request, false);
        //获取statusCode:401,404,500
        Integer statusCode = (Integer) attr.get("status");
        logger.info("异常捕获 请求 状态码: "+String.valueOf(statusCode));
        switch (statusCode) {
            case 401:
                return failure("当前没有访问权限 " + attr.get("message"), statusCode);
            case 500:
                return failure("服务器内部错误，无法完成请求 " + attr.get("message"), statusCode);
            case 404:
                return failure("Not Found " + attr.get("message"), statusCode);
            case 403:
                return failure("服务器拒绝访问 " + attr.get("message"), statusCode);
            case 400:
                return failure("客户端请求参数有误 " + attr.get("message"), statusCode);
            case 405:
                return failure("客户端请求中的方法被禁止 " + attr.get("message"), statusCode);
            default:
                return failure("异常 " + attr.get("message"), statusCode);
        }

    }


    @Autowired
    public GlobalExceptionHandler(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
