package com.example.opensys.base;

import com.alibaba.fastjson.JSONObject;

public class ResultBeanModel {
    private int code;
    private String msg;
    private Object data;
    private static ResultBeanModel resultBean;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static JSONObject getResultBean(int code, String msg, Object data) {

        if (resultBean==null){
            resultBean = new ResultBeanModel(code,msg,data);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",code);
        jsonObject.put("msg",msg);
        jsonObject.put("data",data);

        return jsonObject;
    }

    public ResultBeanModel() {

    }

    public ResultBeanModel(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}