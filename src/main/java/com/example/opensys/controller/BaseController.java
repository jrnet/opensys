package com.example.opensys.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.opensys.base.ResultBeanModel;
import com.example.opensys.utils.JsonObjectUtils;

public class BaseController {
    protected JSONObject success(Object data) {

        return setJosnData(data, "成功", 1);
    }

    protected JSONObject success(Object data, String msg) {

        return setJosnData(data, msg, 1);
    }

    protected JSONObject success(String msg)
    {
        return setJosnData(null, msg, 1);
    }

    protected JSONObject success()
    {
        return setJosnData(null, "成功", 1);
    }
    protected JSONObject failure(String msg) {

        return setJosnData(null, msg, 0);
    }

    protected JSONObject failure(Object data) {
        return setJosnData(data, "失败", 0);
    }

    protected JSONObject failure(String msg, Object data) {

        return setJosnData(data, msg, 0);
    }

    protected JSONObject failure(String msg, Integer status) {

        return setJosnData(null, msg, status);
    }
    protected JSONObject failure() {

        return setJosnData(null, "失败", 0);
    }


    private JSONObject setJosnData(Object data, String msg, int status) {
        ResultBeanModel dataModel = new ResultBeanModel();
        dataModel.setData(data);
        dataModel.setMsg(msg);
        dataModel.setCode(status);
        return JsonObjectUtils.jsonDataModelToJson(dataModel);
    }
}
