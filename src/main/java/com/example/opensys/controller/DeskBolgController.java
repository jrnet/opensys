package com.example.opensys.controller;

import com.example.opensys.model.BlogBookModel;
import com.example.opensys.seivices.DeskBlogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController()
@RequestMapping(value = "/blog", method = RequestMethod.GET)
public class DeskBolgController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(SysController.class);

    @Autowired
    private DeskBlogService deskBlogService;

    /**
     * 获取全部文章列表
     *
     * @param page    页码
     * @param limit   条数
     * @param title   标题
     * @param content 内容
     * @return
     */
    @RequestMapping(value = "/getBlogAll")
    @ResponseBody
    public JSONObject getBlogList(@RequestParam(required = true) @NotNull(message = "page不能为空") Integer page,
                                  @RequestParam(required = true) @NotNull(message = "limit不能为空") Integer limit,
                                  @RequestParam(required = false) String title,
                                  @RequestParam(required = false) String content,
                                  HttpServletRequest httpServletRequest
    ) {
        return success(deskBlogService.finBlogBookList(page, limit, title, content), "ok");
    }

    /**
     * 查看详情
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getBlogInfo")
    @ResponseBody
    public JSONObject getBlogInfo(@RequestParam(required = true) @NotBlank(message = "id不能为空") String id) {
        return success(deskBlogService.finBlogBookInfo(id), "ok");
    }


    /**
     * 创作
     * @param blogBookModel
     * @return
     */
    @RequestMapping(value = "/sys/addBlog",method = RequestMethod.POST)
    @ResponseBody
    public JSONObject addBlog(@RequestBody @Valid BlogBookModel blogBookModel) {
        if (blogBookModel.getTitle().length()>32){
            return failure("标题过长，请重新输入");
        }
        if (deskBlogService.addBlogBook(blogBookModel) == 1){
            return success();
        } else {
            return failure();
        }
    }

    /**
     * 获取全部文章列表
     *
     * @param page    页码
     * @param limit   条数
     * @param title   标题
     * @param content 内容
     * @return
     */
    @RequestMapping(value = "/sys/getBlogAll")
    @ResponseBody
    public JSONObject getSysBlogList(@RequestParam(required = true) @NotNull(message = "page不能为空") Integer page,
                                  @RequestParam(required = true) @NotNull(message = "limit不能为空") Integer limit,
                                  @RequestParam(required = false) String title,
                                  @RequestParam(required = false) String content,
                                  HttpServletRequest httpServletRequest
    ) {
        return success(deskBlogService.finBlogBookList(page, limit, title, content), "ok");
    }

    /**
     * 删除博客
     * @param ids
     * @return
     */
    @RequestMapping(value = "/sys/delBlog",method = RequestMethod.POST)
    @ResponseBody
    public JSONObject delBlog(@RequestBody(required = true) @NotNull(message = "ids不能为空") List<String> ids) {
        if (deskBlogService.delBlogBookInfo(ids) != null){
            return success();
        }else{
            return failure("文件已删除或不存在！");
        }
    }


}
