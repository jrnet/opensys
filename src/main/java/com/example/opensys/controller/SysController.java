package com.example.opensys.controller;
import com.alibaba.fastjson.JSONObject;
import com.example.opensys.model.SystemTokenModel;
import com.example.opensys.model.UserModel;
import com.example.opensys.model.dto.UserModelDTO;
import com.example.opensys.model.po.UserPo;
import com.example.opensys.seivices.SysService;
import com.example.opensys.seivices.UserService;
import com.example.opensys.utils.VerifyCodeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Validated
@RestController
@RequestMapping(value = "/sys")
public class SysController extends BaseController{

    private static final Logger logger = LoggerFactory.getLogger(SysController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private SysService sysService;

    /**
     * 验证码
     * @param request
     * @param response
     */
    @RequestMapping(value = "/code",method={RequestMethod.GET})
    public void getCode(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/jpeg");

            // 生成随机字串
            String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
            // 存入会话session
            HttpSession session = request.getSession(true);
            // 删除以前的
            session.removeAttribute("verCode");
            session.removeAttribute("codeTime");
            session.setAttribute("verCode", verifyCode.toLowerCase());
            session.setAttribute("codeTime", LocalDateTime.now());
            // 生成图片
            int w = 200, h = 50;
            OutputStream out = response.getOutputStream();
            VerifyCodeUtils.outputImage(w, h, out, verifyCode);
        } catch (Exception e) {
            System.out.println("验证码获取错误");
            System.out.println(e);
        }

    }

    /**
     * 登录
     * @return
     */
    @RequestMapping(value = "/login",method={RequestMethod.POST})
    @ResponseBody
    public JSONObject login(@RequestBody @Valid UserModelDTO user, HttpSession session) {
        String errUserPwd = "用户名或密码错误";
        String errCode1 = "验证码已失效，请重新输入";
        String errCode2 = "验证码有误，请重新输入";
        // 校验验证码
        Object verCode = session.getAttribute("verCode");
        if (null == verCode) {
            return failure(errCode1);
        }

        String verCodeStr = verCode.toString();
        if (verCodeStr.equals(user.getCode())) {
            //验证成功，删除存储的验证码
            session.removeAttribute("verCode");
            session.removeAttribute("codeTime");
        }else{
            return failure(errCode2);
        }

        // 校验用户名密码
        UserModel userBean = userService.finUserName(user.getUsername());

        // 用户名不存在
        if (userBean == null) {
            return failure(errUserPwd);
        }else{
            // 密码有误
            if (!user.getPassword().equals(userBean.getPassword())){
                return failure(errUserPwd);
            }else{
                return success(sysService.createToken(userBean.getUid()),"登录成功");
            }
        }
    }


    /**
     * 注册
     * @param userBo
     * @param bindingResult
     * @return
     */
    @ApiOperation(value="发送解析文本", notes="发送解析文本", produces="application/json")
    @RequestMapping(value = "/reg",method={RequestMethod.POST})
    public JSONObject register(@Valid UserModel userBo, BindingResult bindingResult) {
        // 校验用户名是否存在
        UserModel  u = userService.finUserName(userBo.getUsername());
        if (u == null){
            return  failure("用户名已存在");
        }
        UserModel user = new UserModel();
        user.setUsername(userBo.getUsername());
        user.setPassword(userBo.getPassword());
        user.setSex(userBo.getSex());
        user.setEmail(userBo.getEmail());
        user.setQq(userBo.getQq());
        user.setImgpath(userBo.getImgpath());
        user.setVer(userBo.getVer());
        userService.addUser(userBo);

        return success("注册成功");
    }


    /**
     * 获取用户列表
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/userAll",method={RequestMethod.GET})
    public JSONObject userList() throws IOException {
        List<UserPo> userList = userService.finUserList();
        return  success(userList);
    }


    /**
     * 没有token
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/notToken",method={RequestMethod.GET,RequestMethod.POST})
    public JSONObject notToken() throws IOException {
        return  failure("token不能为空",0);
    }

}
