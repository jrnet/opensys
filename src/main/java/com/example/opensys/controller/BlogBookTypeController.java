package com.example.opensys.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.opensys.model.BlogBookTypeModel;
import com.example.opensys.seivices.BlogBookTypeService;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
@RestController()
@RequestMapping(value = "/blog/type", method = RequestMethod.GET)
public class BlogBookTypeController extends  BaseController{


    private static final Logger logger = LoggerFactory.getLogger(SysController.class);

    @Autowired
    private BlogBookTypeService blogBookTypeService;

    /**
     * 获取全部文章类型
     * @return 全部文章
     */
    @RequestMapping(value = "/all")
    @ResponseBody
    public JSONObject bookType() {
        List<BlogBookTypeModel>  list = blogBookTypeService.finAllBlogBookType();
        if ( list!= null){
            return success(list);
        }else{
            return failure();
        }
    }

    /**
     * 获取全部文章类型-分页
     * @param page 页码
     * @param limit 数量
     * @return
     */
    @RequestMapping(value = "/allPage")
    @ResponseBody
    public JSONObject bookType(@NotBlank(message = "页码不能为空") Integer page,@NotBlank(message = "数量不能为空") Integer limit) {
        PageInfo<BlogBookTypeModel> list = blogBookTypeService.finAllBlogBookTypePage(page, limit);
        if ( list!= null){
            return success(list);
        }else{
            return failure();
        }
    }

    /**
     * 修改类型状态
     * @param id 类型id
     * @param status 2=不可用，3=可用
     * @return
     */
    @RequestMapping(value = "/status")
    @ResponseBody
    public JSONObject bookStatus(@RequestParam @NotNull(message = "id不嫩为空")String id, @RequestParam @NotNull(message = "id不嫩为空")Integer status) {
        Integer list = blogBookTypeService.updateBookStatus(id, status);
        if ( list!= null){
            return success(list);
        }else{
            return failure();
        }
    }

    /**
     * 添加类型
     * @param label 类别名称
     * @return
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public JSONObject addType(@RequestParam @NotNull(message = "id不嫩为空")String label) {
        Integer list = blogBookTypeService.addType(label);
        if ( list!= null){
            return success(list);
        }else{
            return failure();
        }
    }

    /**
     * 获取单个文章类型
     * @param id 类型id
     * @return
     */
    @RequestMapping(value = "/One")
    @ResponseBody
    public JSONObject bookOne(@RequestParam(required = true) @NotBlank(message = "id不能为空") String id) {
        BlogBookTypeModel  blogBookTypeModel = blogBookTypeService.finBlogBookTypeOne(id);
        if ( blogBookTypeModel!= null){
            return success(blogBookTypeModel);
        }else{
            return failure();
        }
    }

    /**
     * 获取单个删除
     * @param ids 类型id
     * @return
     */
    @RequestMapping(value = "/del",method = RequestMethod.POST)
    @ResponseBody
    public JSONObject delBlog(@RequestBody(required = true) @NotNull(message = "ids不能为空") List<String> ids) {
        if (blogBookTypeService.blogDels(ids) != null){
            return success();
        }else{
            return failure("文件已删除或不存在！");
        }
    }
}
