package com.example.opensys.model;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "system_token")
public class SystemTokenModel {
    @Column(name = "id")
    private String id;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建用户Id
     */
    @Column(name = "create_user_id")
    private String createUserId;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 修改用户Id
     */
    @Column(name = "update_user_id")
    private String updateUserId;

    /**
     * 用户Id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 登录时间
     */
    @Column(name = "login_time")
    private Date loginTime;

    /**
     * 退出时间
     */
    @Column(name = "logout_time")
    private Date logoutTime;

    /**
     * token
     */
    @Column(name = "token")
    private String token;

    /**
     * 设备类型 1:安卓 2:ios
     */
    @Column(name = "os")
    private String os;

    /**
     * app版本
     */
    @Column(name = "ver")
    private String ver;

    /**
     * 设备Id
     */
    @Column(name = "device_id")
    private String deviceId;

    /**
     * 开发软件版本
     */
    @Column(name = "sdk_int")
    private String sdkInt;

    /**
     * 状态 1:已登录 2:已注销
     */
    @Column(name = "status")
    private Integer status;

    /**
     * 最后请求时间
     */
    @Column(name = "last_request_time")
    private Date lastRequestTime;
}