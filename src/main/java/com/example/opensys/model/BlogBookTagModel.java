package com.example.opensys.model;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "blog_book_tag")
public class BlogBookTagModel {
    /**
     * id
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 标签名
     */
    @Column(name = "label")
    private String label;

    /**
     * 创建时间
     */
    @Column(name = "creatDateTime")
    private Date creatdatetime;

    /**
     * 更新时间
     */
    @Column(name = "updateDateTime")
    private Date updatedatetime;

    /**
     * 是否删除  0已删除，1未删除
     */
    @Column(name = "state")
    private Integer state;

    /**
     * 创建人
     */
    @Column(name = "creatUid")
    private String creatuid;

    /**
     * 更新人
     */
    @Column(name = "updateUid")
    private String updateuid;
}