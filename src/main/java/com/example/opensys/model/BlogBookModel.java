package com.example.opensys.model;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "blog_book")
public class BlogBookModel {
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 标题
     */
    @Column(name = "title")
    private String title;

    /**
     * 封面
     */
    @Column(name = "pic")
    private String pic;

    @Column(name = "creatDateTime")
    private Date creatdatetime;

    @Column(name = "updateDateTime")
    private Date updatedatetime;

    @Column(name = "state")
    private Integer state;

    @Column(name = "creatUid")
    private String creatuid;

    @Column(name = "updateUid")
    private String updateuid;

    /**
     * 内容
     */
    @Column(name = "content")
    private String content;

    /**
     * 分类
     */
    @Column(name = "type")
    private String type;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 原创
     */
    @Column(name = "original")
    private Integer original;

    public Integer getIsOriginal() {
        return original;
    }

    public void getIsOriginal(Integer original) {
        this.original = original;
    }

//    不序列化
    @Transient
    private String realName;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Date getCreatdatetime() {
        return creatdatetime;
    }

    public void setCreatdatetime(Date creatdatetime) {
        this.creatdatetime = creatdatetime;
    }

    public Date getUpdatedatetime() {
        return updatedatetime;
    }

    public void setUpdatedatetime(Date updatedatetime) {
        this.updatedatetime = updatedatetime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCreatuid() {
        return creatuid;
    }

    public void setCreatuid(String creatuid) {
        this.creatuid = creatuid;
    }

    public String getUpdateuid() {
        return updateuid;
    }

    public void setUpdateuid(String updateuid) {
        this.updateuid = updateuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}