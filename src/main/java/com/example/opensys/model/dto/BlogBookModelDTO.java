package com.example.opensys.model.dto;

import com.example.opensys.model.BlogBookModel;

public class BlogBookModelDTO extends BlogBookModel {
    private String realName;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
