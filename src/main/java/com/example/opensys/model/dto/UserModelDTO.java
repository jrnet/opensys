package com.example.opensys.model.dto;

import com.example.opensys.model.UserModel;

import javax.validation.constraints.NotNull;

public class UserModelDTO extends UserModel {

    @NotNull(message = "验证码不能为空")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
