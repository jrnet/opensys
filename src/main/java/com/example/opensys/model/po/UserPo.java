package com.example.opensys.model.po;

import com.example.opensys.model.SystemTokenModel;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
public class UserPo {
    @Column(name = "uid")
    private String uid;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    @NotNull(message = "用户名不能为空")
    private String username;

    @Column(name = "sex")
    private Integer sex;

    @Column(name = "qq")
    private String qq;

    @Column(name = "email")
    private String email;

    @Column(name = "ver")
    private String ver;

    @Column(name = "state")
    private Integer state;

    @Column(name = "imgPath")
    private String imgpath;

    private SystemTokenModel token;

}
