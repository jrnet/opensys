/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : opensys

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 13/11/2020 17:57:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog_book
-- ----------------------------
DROP TABLE IF EXISTS `blog_book`;
CREATE TABLE `blog_book`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '内容',
  `pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '封面',
  `creatDateTime` datetime(0) NULL DEFAULT NULL,
  `updateDateTime` datetime(0) NULL DEFAULT NULL,
  `state` int(1) NULL DEFAULT NULL,
  `creatUid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `updateUid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blog_book
-- ----------------------------
INSERT INTO `blog_book` VALUES ('123', '可以通过向 axios 传递相关配置来创建请求', '<pre><code>// 发送 POST 请求<br>axios({<br>  method: \'post\',<br>  url: \'/user/12345\',<br>  data: {<br>    firstName: \'Fred\',<br>    lastName: \'Flintstone\'<br>  }<br>});</code></pre><p><br></p>', NULL, '2020-08-31 17:44:57', '2020-09-03 17:44:59', 1, '1111', NULL);
INSERT INTO `blog_book` VALUES ('1234', '111可以通过向 axios 传递相关配置来创建请求', '<pre><code>// 发送 POST 请求<br>axios({<br>  method: \'post\',<br>  url: \'/user/12345\',<br>  data: {<br>    firstName: \'Fred\',<br>    lastName: \'Flintstone\'<br>  }<br>});</code></pre><p><br></p>', NULL, '2020-08-31 17:44:57', '2020-09-03 17:44:59', 1, '1111', NULL);
INSERT INTO `blog_book` VALUES ('12345', '222可以通过向 axios 传递相关配置来创建请求', '<pre><code>// 发送 POST 请求<br>axios({<br>  method: \'post\',<br>  url: \'/user/12345\',<br>  data: {<br>    firstName: \'Fred\',<br>    lastName: \'Flintstone\'<br>  }<br>});</code></pre><p><br></p>', NULL, '2020-08-31 17:44:57', '2020-09-03 17:44:59', 1, '1111', NULL);
INSERT INTO `blog_book` VALUES ('123456', '555可以通过向 axios 传递相关配置来创建请求', '<pre><code>// 发送 POST 请求<br>axios({<br>  method: \'post\',<br>  url: \'/user/12345\',<br>  data: {<br>    firstName: \'Fred\',<br>    lastName: \'Flintstone\'<br>  }<br>});</code></pre><p><br></p>', NULL, '2020-08-31 17:44:57', '2020-09-03 17:44:59', 1, '1111', NULL);

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建用户Id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '修改用户Id',
  `user_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户Id',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `logout_time` datetime(0) NULL DEFAULT NULL COMMENT '退出时间',
  `token` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'token',
  `os` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '设备类型 1:安卓 2:ios',
  `ver` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'app版本',
  `device_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '设备Id',
  `sdk_int` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '开发软件版本',
  `status` tinyint(3) NULL DEFAULT NULL COMMENT '状态 1:已登录 2:已注销',
  `last_request_time` datetime(0) NULL DEFAULT NULL COMMENT '最后请求时间'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_token
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `uid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `sex` int(1) NULL DEFAULT NULL,
  `qq` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `ver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `imgPath` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `state` int(1) NULL DEFAULT NULL,
  `insertDateTime` datetime(0) NOT NULL,
  `updateDateTime` datetime(0) NULL DEFAULT NULL,
  `insertUser` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `updateUser` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1111', '李白', '123', '123', 1, '123', '111@qqq', '此人很懒，啥也没写~', NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL);
INSERT INTO `user` VALUES ('1112', '李黑', '111', '111', 1, '23', '111@qq.com', '此人很懒，啥也没写~', '', 1, '0000-00-00 00:00:00', NULL, NULL, NULL);
INSERT INTO `user` VALUES ('1113', '张三丰', 'root', 'root', 1, '123456', 'root@root.com', '此人很懒，啥也没写~', NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
